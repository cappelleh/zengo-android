# ZenGo

## About this App

Free and open source App to connect with public API for Zer0 next gen SR/F and SR/S electr1c m0t0rcycles.
This builds upon the information you see when using the "remote connect" functionality in the official app.
Showing more data and adding more configuration options and features while keeping source available. 

Non Zer0 SR-platform users can benefit from the reference manual in the app containing general information 
about the different platforms, firmware versions and recalls. And the range calculator since that has been 
updated to support different battery configurations and conditions. 

Android app is available from the Google Play Store, 
see https://play.google.com/store/apps/details?id=be.hcpl.android.zengo

![Android screenshot of overview](https://bitbucket.org/cappelleh/zengo/raw/3ec93942dd2c4aaf1885590801e1d2debc36f7a1/screenshots/Screenshot_1575383909_resized.png)
![Android screenshot of range estimator](https://bitbucket.org/cappelleh/zengo/raw/3ec93942dd2c4aaf1885590801e1d2debc36f7a1/screenshots/Screenshot_1575383914_resized.png)
![Android screenshot of bike location](https://bitbucket.org/cappelleh/zengo/raw/3ec93942dd2c4aaf1885590801e1d2debc36f7a1/screenshots/Screenshot_1575383917_resized.png)

iOS app is available from this App Store link
https://apps.apple.com/be/app/zerong/id1488172044?l=nl

![iPhone 11 screenshot of overview](https://bitbucket.org/cappelleh/zengo/raw/75c1821fa599044dfc6867b1338d139529372f59/screenshots/resized_Simulator%20Screen%20Shot%20-%20iPhone%2011%20Pro%20Max%20-%202019-11-23%20at%2009.06.11.png)
![iPhone 11 screenshot of range estimator](https://bitbucket.org/cappelleh/zengo/raw/75c1821fa599044dfc6867b1338d139529372f59/screenshots/resized_Simulator%20Screen%20Shot%20-%20iPhone%2011%20Pro%20Max%20-%202019-11-23%20at%2009.06.16.png)
![iPhone 11 screenshot of bike location](https://bitbucket.org/cappelleh/zengo/raw/75c1821fa599044dfc6867b1338d139529372f59/screenshots/resized_Simulator%20Screen%20Shot%20-%20iPhone%2011%20Pro%20Max%20-%202019-11-23%20at%2009.06.20.png)

## technical Improvements

```
Warning:(5, 27) 'PreferenceManager' is deprecated. Deprecated in Java
Warning:(30, 5) Use `SwitchCompat` from AppCompat or `SwitchMaterial` from Material library
Warning:(58, 35) 'getDefaultSharedPreferences(Context!): SharedPreferences!' is deprecated. Deprecated in Java
Warning:(352, 27) To get local formatting use `getDateInstance()`, `getDateTimeInstance()`, or `getTimeInstance()`, or use `new SimpleDateFormat(String template, Locale locale)` with for example `Locale.US` for ASCII dates.
```

## App Version History

### 3.1

- update target SDK

### 3.0

removed all Zer0 references from app after app take down by their legal department

- no more link to Zer0 app within app menu
- renamed app to ZeNGo
- some code cleanup

### 2.6

- fix for small screen rendering of range estimator view
- removed fab since feedback option also available from menu
- routes now printed in random color changing for each day/fetch
- made random color route an option (on by default)
- fix show/hide clear option from location fragment
- show total distance for plotted routes on map

### 2.5

- add password reset information to reference documentation
- add specific vin details on SR/F vs SR/S model

### 2.4

- fixed keep screen on feature

### 2.3

- bugfix added reveal password option to layout group
- added keep screen on feature on user request

### 2.2

- allow showing current password
- show last timestamp in current timezone and formatted on user request
- added warning about 14.4kWh battery pack based to range calculator
- added Zer0 platform breakdown to reference manual
- added firmware upgrade instruction to reference manual
- added recall information to reference manual

### 2.1

- date fixed
- recalc on resume fixed

### 2.0

- small code and layout improvements
- added datepicker to map view to render a route for selected date

### 1.2

- demo login added for new users w/o Zer0
- dutch translations added
- small screen improvements for range and map

### 1.1

- fixed keyboard open on resume blocking overview
- fixed no result shown on resume of range calculator
- text fixed on reference documentation
- fixed calculation of range falling back to default value

### 1.0

- range estimation tool added
- several layout fixes
- show location on map including current range
- reduced required network calls on resume
- app about information added to reference
- show weather forecast on map
- fix crash on quickly changing tabs

### 0.4

- most important specifications added to help section
- VIN breakdown added to reference section
- linked to Zer0 nextgen app and gates belt tension app
- about section added to app
- tabs prepared for range estimation and map views
- layouts aligned with iOS layout

### 0.3

- show location of bike on map
- circular progress for SoC
- improved data visualisation

### 0.2

- fingerprint implementation to store password securely
- loading animation on getting data
- required network traffic for refresh reduced
- visualise error messages from network calls

### 0.1

Initial release with minimum functionality. Required user to enter username and password on each use of app for example. 


## Resources

### API discovery as posted on electricmotorcyleforum.com

Remote service was discovered and published on this forum dedicated to electric motorcycles: https://electricmotorcycleforum.com/boards/index.php?topic=9520.0

> Turns out you can also use a plain URL in your browser :
>
> https://mongol.brono.com/mongol/api.php?commandname=get_units&format=json&user=yourusername&pass=yourpass
>
> to get your unitnumber you have to use in the URL below
>
> https://mongol.brono.com/mongol/api.php?commandname=get_last_transmit&format=json&user=yourusername&pass=yourpass&unitnumber=0000000

### Documentation from starcom systems

See this published document (also in the downloads section of this app) with information on how
starcom systems colaborated with Zer0 motorcycles to create the next gen connected system:
https://www.starcomsystems.com/wp-content/uploads/2019/05/The-Worlds-First-Connected-Motorcycle.pdf

So this system is based on the helios system that starcom sells. For documentation on that helios syste check: https://www.starcomsystems.com/helios/

Starcom also released this blog post on working with Zer0 and the results (again can be downloaded as a PDF from download section):
https://www.starcomsystems.com/2019/03/28/the-first-smart-connected-motorcycle-effortless-connection/

### Official Zer0 Resources

See their website - LINK REMOVED

## The API, what we know so far

To get json information back you can query using https://mongol.brono.com/mongol/api.php?commandname=get_units&format=json&user=yourusername&pass=yourpass first to get the unitnumber that applies to your configuration.
Once you have that you can trigger other commands like `get_userinfo`, `get_last_transmit`, `get_commands` and `get_history`.

## Other Apps

Official Zer0 app for pre next gen motorcycles is and for Next gen motorcycles is available in Google Play Store and Apple App Store. LINKS REMOVED

Starcom have this app https://play.google.com/store/apps/details?id=com.starcomsystems.olympiatracking
and this one https://play.google.com/store/apps/details?id=com.starcomsystems.olympiarc

ZeroSpy is an Android app that supports the original zer0 system https://play.google.com/store/apps/details?id=com.bsc101.zerospy

A collection of other open source projects from Zer0 Enthousiasts https://github.com/zero-motorcycle-community

### Used Libraries

- retrofit for network calls https://square.github.io/retrofit/
- icon generator https://romannurik.github.io/AndroidAssetStudio (also part of android studio)
- markdown implementation for Android https://github.com/noties/Markwon

## Contributions

### How to become a test user

Both Android and iOS versions of the app have a beta test version released just before the actual full release goes live. This is done to exclude bigger issues to be released in the production version.

**For iOS** you can become a test user by installing the [test flight app from the app store](https://apps.apple.com/us/app/testflight/id899247664) and then sending me an e-mail at hans-AT-hcpl.be to ask for an invite. Once the invite is accepted you'll receive an update on the given e-mail address and you'll find the test app in the testflight app where you can install or remove it. This overrides the production app you might already have installed. Information shouldn't be lost and you should be able to switch between test and production whenever you want, as longs as there is a test version available.

**For Android** you can [register yourself using this link](https://play.google.com/apps/testing/be.hcpl.android.zengo). Once that is done you'll find an update for the app in the Google Play Store on your device. It's possible that you have to restart the play store app to receive this update.

### How to contribute code

Contributions are more than welcome. If you can't program there are plenty other options like providing translations, filing bugs, creating feature requests, ...