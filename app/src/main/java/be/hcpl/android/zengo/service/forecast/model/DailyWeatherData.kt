package be.hcpl.android.zengo.service.forecast.model

import androidx.annotation.Keep

@Keep
class DailyWeatherData : WeatherData() {

    var precipIntensityMax: Float = 0.toFloat()
    var sunriseTime: Long = 0
    var sunsetTime: Long = 0
    var temperatureMin: Float = 0.toFloat()
    var temperatureMinTime: Long = 0
    var temperatureMax: Float = 0.toFloat()
    var temperatureMaxTime: Long = 0
    var apparentTemperatureMin: Float = 0.toFloat()
    var apparentTemperatureMinTime: Long = 0
    var apparentTemperatureMax: Float = 0.toFloat()
    var apparentTemperatureMaxTime: Long = 0

}
