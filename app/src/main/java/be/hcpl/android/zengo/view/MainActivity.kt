package be.hcpl.android.zengo.view

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuItem
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import be.hcpl.android.zengo.R
import be.hcpl.android.zengo.service.starcom.model.NextGenTransmit
import be.hcpl.android.zengo.service.starcom.model.NextGenUnit
import com.google.android.material.tabs.TabLayout


class MainActivity : AppCompatActivity() {

    private lateinit var prefs: SharedPreferences

    private var overview = OverviewFragment()
    private var reference = ReferenceFragment()
    private var range = RangeFragment()
    private var location = LocationFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        prefs = PreferenceManager.getDefaultSharedPreferences(this)

        // show initial view
        supportFragmentManager.beginTransaction().replace(
            R.id.content,
            overview
        ).commit()

        // handle tab changes
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> supportFragmentManager.beginTransaction().replace(
                        R.id.content,
                        overview
                    ).commit()
                    1 -> supportFragmentManager.beginTransaction().replace(
                        R.id.content,
                        reference
                    ).commit()
                    2 -> supportFragmentManager.beginTransaction().replace(
                        R.id.content,
                        range
                    ).commit()
                    3 -> supportFragmentManager.beginTransaction().replace(
                        R.id.content,
                        location
                    ).commit()
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                // ignore
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                // ignore
            }

        })
    }

    override fun onResume() {
        super.onResume()
        if( prefs.getBoolean(OverviewFragment.KEY_SCREEN_ON, false) ){
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_belt_app -> {
                openApp("com.gates.carbondrivecalculator")
                true
            }
            R.id.action_about -> {
                openWeb("https://bitbucket.org/cappelleh/zengo/src/master/")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openWeb(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    private fun openApp(appId: String) {
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=".plus(appId))
                )
            )
        } catch (anfe: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=".plus(appId))
                )
            )
        }
    }

    // keep some state
    data class State(val unit: NextGenUnit? = null, val transmit: NextGenTransmit? = null, val calculatedRange: Double? = 150.0)
    var state: State = State()

}
