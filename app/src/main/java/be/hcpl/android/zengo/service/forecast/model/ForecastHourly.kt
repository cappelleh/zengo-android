package be.hcpl.android.zengo.service.forecast.model

import androidx.annotation.Keep

@Keep
class ForecastHourly {

    var summary: String? = null
    var icon: String? = null
    var data: Array<HourlyWeatherData>? = null

}
