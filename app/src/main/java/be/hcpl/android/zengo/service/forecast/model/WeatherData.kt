package be.hcpl.android.zengo.service.forecast.model

import androidx.annotation.Keep

@Keep
open class WeatherData {

    var time: Long = 0
    var summary: String? = null
    var icon: String? = null
    var precipIntensity: Float = 0.toFloat()
    var precipProbability: Float = 0.toFloat()

    var dewPoint: Float = 0.toFloat()
    var windSpeed: Float = 0.toFloat()
    var windBearing: Int = 0
    var cloudCover: Float = 0.toFloat()
    var humidity: Float = 0.toFloat()
    var pressure: Float = 0.toFloat()
    var visibility: Float = 0.toFloat()
    var ozone: Float = 0.toFloat()

}
