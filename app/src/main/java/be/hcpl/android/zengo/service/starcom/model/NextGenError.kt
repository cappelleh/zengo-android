package be.hcpl.android.zengo.service.starcom.model

import androidx.annotation.Keep

@Keep
data class NextGenError(val error: String)
