package be.hcpl.android.zengo.service.forecast.model

import androidx.annotation.Keep

@Keep
class ForecastResponse {

    var latitude: Double = 0.toDouble()
    var longitude: Double = 0.toDouble()
    var timezone: String? = null
    var offset: Int = 0
    var currently: HourlyWeatherData? = null
    var hourly: ForecastHourly? = null
    var daily: ForecastDaily? = null
    var flags: ResponseFlags? = null

}
