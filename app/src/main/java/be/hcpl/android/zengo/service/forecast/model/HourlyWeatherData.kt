package be.hcpl.android.zengo.service.forecast.model

import androidx.annotation.Keep

@Keep
class HourlyWeatherData : WeatherData() {

    var temperature: Float = 0.toFloat()
    var apparentTemperature: Float = 0.toFloat()

}
