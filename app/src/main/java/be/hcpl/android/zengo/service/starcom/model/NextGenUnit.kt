package be.hcpl.android.zengo.service.starcom.model

import androidx.annotation.Keep

@Keep
data class NextGenUnit(
    val unitnumber: Long?,
    val name: String? = "default",
    val address: String? = "unknown",
    val vehiclemodel: String? = "unknown",
    val vehiclecolor: String? = "unknown",
    val unittype: Int? = 0,
    val icon: Int? = 0,
    val active: Int? = 0,
    val unitmodel: Int? = 0
)

// TODO custom array at the end, no values received so no clue on structure so far, probably key/value pairs?
