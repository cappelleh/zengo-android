package be.hcpl.android.zengo.view

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.SeekBar
import android.widget.Spinner
import android.widget.Switch
import android.widget.TextView
import androidx.fragment.app.Fragment
import be.hcpl.android.zengo.R

class RangeFragment : Fragment(R.layout.fragment_range) {

    // for last used values
    private lateinit var prefs: SharedPreferences

    // default range values
    private val defaultRangeValues = arrayOf(287, 216, 193, 174, 143)
    var selectedRoadTypeIndex: Int = 0
    var stateOfChargePercentage: Int = 65

    private lateinit var spinner: Spinner
    private lateinit var slider: SeekBar
    private lateinit var socView: TextView
    private lateinit var ridingSwitch: Switch
    private lateinit var landscapeSwitch: Switch
    private lateinit var selectCold: Button
    private lateinit var selectMild: Button
    private lateinit var selectWarm: Button
    private lateinit var secondaryRange: TextView
    private lateinit var primaryRange: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        spinner = view.findViewById(R.id.spinner)
        slider = view.findViewById(R.id.slider)
        socView = view.findViewById(R.id.label_soc)
        landscapeSwitch = view.findViewById(R.id.switch_landscape)
        ridingSwitch = view.findViewById(R.id.switch_riding)
        selectCold = view.findViewById(R.id.select_cold)
        selectMild = view.findViewById(R.id.select_mild)
        selectWarm = view.findViewById(R.id.select_warm)
        secondaryRange = view.findViewById(R.id.secondary_range_value)
        primaryRange = view.findViewById(R.id.primary_range_value)

        onViewCreatedCont()
    }

    private fun onViewCreatedCont() {

        // get user preferences
        prefs = PreferenceManager.getDefaultSharedPreferences(requireContext())

        // populate spinner with road types
        val adapter = ArrayAdapter.createFromResource(requireContext(), R.array.roadTypes, R.layout.spinner_item)
        spinner.adapter = adapter
        spinner.setSelection(prefs.getInt(KEY_RANGE_ROAD_TYPE, 0))
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // what should we do when user has no selection?
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedRoadTypeIndex = position
                prefs.edit().putInt(KEY_RANGE_ROAD_TYPE, position).apply()
                calculateRangeLeft()
            }

        }

        // restore slider value from current API soc value instead
        val transmitSoc = (requireActivity() as MainActivity).state.transmit?.soc
        stateOfChargePercentage = if ( transmitSoc != null && transmitSoc > 0 && transmitSoc < 100) transmitSoc else 65
        slider.progress = stateOfChargePercentage
        socView.text = getString(R.string.value_soc, slider.progress)
        slider.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                stateOfChargePercentage = progress
                socView.text = getString(R.string.value_soc, progress)
                calculateRangeLeft()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                // no need for
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // no need for
            }
        })

        // restore input from previous sessions
        ridingSwitch.isChecked = prefs.getBoolean(KEY_RANGE_RIDING, false)
        landscapeSwitch.isChecked = prefs.getBoolean(KEY_RANGE_LANDSCAPE, false)
        selectCold.isSelected = prefs.getBoolean(KEY_RANGE_WEATHER_COLD, false)
        selectMild.isSelected = prefs.getBoolean(KEY_RANGE_WEATHER_MILD, false)
        selectWarm.isSelected = !selectMild.isSelected && !selectCold.isSelected

        // store values when these change and recalculate result
        ridingSwitch.setOnCheckedChangeListener { _, isChecked ->
            prefs.edit().putBoolean(KEY_RANGE_RIDING, isChecked).apply()
            calculateRangeLeft()
        }
        landscapeSwitch.setOnCheckedChangeListener { _, isChecked ->
            prefs.edit().putBoolean(KEY_RANGE_LANDSCAPE, isChecked).apply()
            calculateRangeLeft()
        }
        selectCold.setOnClickListener {
            selectCold.isSelected = true
            selectMild.isSelected = false
            selectWarm.isSelected = false
            prefs.edit().putBoolean(KEY_RANGE_WEATHER_COLD, true)
            .putBoolean(KEY_RANGE_WEATHER_MILD, false).apply()
            calculateRangeLeft()
        }
        selectMild.setOnClickListener {
            selectCold.isSelected = false
            selectMild.isSelected = true
            selectWarm.isSelected = false
            prefs.edit().putBoolean(KEY_RANGE_WEATHER_COLD, false)
                .putBoolean(KEY_RANGE_WEATHER_MILD, true).apply()
            calculateRangeLeft()
        }
        selectWarm.setOnClickListener {
            selectCold.isSelected = false
            selectMild.isSelected = false
            selectWarm.isSelected = true
            prefs.edit().putBoolean(KEY_RANGE_WEATHER_COLD, false)
                .putBoolean(KEY_RANGE_WEATHER_MILD, false).apply()
            calculateRangeLeft()
        }
    }

    private fun calculateRangeLeft(){
        val fullRange = defaultRangeValues[selectedRoadTypeIndex]
        var rangeLeft = fullRange * (stateOfChargePercentage/100.0)

        // also apply penalties based on configuration options
        if( ridingSwitch.isChecked )
            rangeLeft *= 0.90
        if( landscapeSwitch.isChecked)
            rangeLeft *= 0.90
        if( selectMild.isSelected)
            rangeLeft *= 0.95
        if( selectCold.isSelected)
            rangeLeft *= 0.90

        // and display result in both units
        if( prefs.getBoolean(OverviewFragment.KEY_UNITS_IMPERIAL, false) ) {
            secondaryRange.text = getString(R.string.value_range_left_km, rangeLeft.toInt())
            primaryRange.text =
                getString(R.string.value_range_left_mi, (rangeLeft * CONVERT_KM_TO_MILES).toInt())
        } else {
            primaryRange.text = getString(R.string.value_range_left_km, rangeLeft.toInt())
            secondaryRange.text =
                getString(R.string.value_range_left_mi, (rangeLeft * CONVERT_KM_TO_MILES).toInt())
        }

        // store in state
        (requireActivity() as MainActivity).state = (requireActivity() as MainActivity).state.copy(calculatedRange = rangeLeft)

    }

    companion object {

        const val CONVERT_KM_TO_MILES = 0.621371192
        const val KEY_RANGE_LANDSCAPE = "range_landscape"
        const val KEY_RANGE_RIDING = "range_riding"
        const val KEY_RANGE_WEATHER_COLD = "range_weather_cold"
        const val KEY_RANGE_WEATHER_MILD = "range_weather_mild"
        const val KEY_RANGE_ROAD_TYPE = "range_road_type"
    }
}
