package be.hcpl.android.zengo.service.forecast;

/**
 * possible unit values
 *
 * @author hcpl
 */
public @interface Units {

    String US = "us";
    String SI = "si";
    String CA = "ca";
    String UK = "uk";
    String AUTO = "auto";

    // check options at https://developer.forecast.io/docs/v2

    // units=[setting]: Return the API response in units other than the default
    // Imperial units. In particular, the following settings are possible:
    // us: The default, as outlined above.
    // si: Returns results in SI units. In particular, properties now have the
    // following units:
    // summary: Any summaries containing temperature or snow accumulation units
    // will have their values in degrees Celsius or in centimeters
    // (respectively).
    // precipIntensity: Millimeters per hour.
    // precipAccumulation: Centimeters.
    // temperature: Degrees Celsius.
    // apparentTemperature: Degrees Celsius.
    // dewPoint: Degrees Celsius.
    // windSpeed: Meters per second.
    // pressure: Hectopascals (which are, conveniently, equivalent to the
    // default millibars).
    // visibility: Kilometers.
    // ca: Identical to si, except that windSpeed is in kilometers per hour.
    // uk: Identical to si, except that windSpeed is in miles per hour, as in
    // the US. (This option is provided because adoption of SI in the UK has
    // been inconsistent.)
    // auto: Selects the relevant units automatically, based on geographic
    // location.

}
