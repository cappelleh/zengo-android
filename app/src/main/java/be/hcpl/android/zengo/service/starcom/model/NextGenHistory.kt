package be.hcpl.android.zengo.service.starcom.model

import androidx.annotation.Keep

@Keep
data class NextGenHistory(
    val mileage: String? = "",
    val longitude: String?,
    val latitude: String?,
    val altitude: String? = "",
    val gps_valid: String? = "",
    val gps_connected: String? = "",
    val satellites: String? = "",
    val velocity: String? = "",
    val heading: String? = ""
)
