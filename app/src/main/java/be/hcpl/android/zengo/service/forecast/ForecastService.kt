package be.hcpl.android.zengo.service.forecast

import be.hcpl.android.zengo.service.forecast.model.ForecastResponse
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by hans on 02/06/2017.
 */

interface ForecastService {

    //https://api.forecast.io/forecast/API_KEY/LATITUDE,LONGITUDE?units=auto

    @GET("/forecast/{API_KEY}/{LATITUDE},{LONGITUDE}")
    fun forecast(
        @Path("API_KEY") apiKey: String,
        @Path("LATITUDE") lat: Double?,
        @Path("LONGITUDE") lon: Double?,
        @Query("units") @Units untis: String
    ): Call<ForecastResponse>

}

class ForecastServiceImpl(private val baseUrl: String) {

    fun getService(): ForecastService {
        return getRetrofit(baseUrl).create(ForecastService::class.java)
    }

    private fun getRetrofit(url: String): Retrofit {
        val gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }
}
