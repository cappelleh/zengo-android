package be.hcpl.android.zengo.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import be.hcpl.android.zengo.R
import io.noties.markwon.Markwon
import java.io.ByteArrayOutputStream
import java.io.InputStream

class ReferenceFragment : Fragment(R.layout.fragment_reference) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // get reference documentation
        val markwon = Markwon.create(requireContext())
        val inputStream = resources.openRawResource(R.raw.reference_information)
        val text = readTextFile(inputStream)
        markwon.setMarkdown(view.findViewById(R.id.reference_text), text)
    }

    private fun readTextFile(inputStream: InputStream): String {
        val outputStream = ByteArrayOutputStream()
        val buff = ByteArray(1024)
        inputStream.buffered().use { input ->
            while (true) {
                val sz = input.read(buff)
                if (sz <= 0) break
                ///at that point we have a sz bytes in the buff to process
                outputStream.write(buff, 0, sz)
            }
        }
        return outputStream.toString()
    }
}
