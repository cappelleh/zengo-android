package be.hcpl.android.zengo.view

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import be.hcpl.android.zengo.R
import be.hcpl.android.zengo.service.starcom.NextGentServiceImpl
import be.hcpl.android.zengo.service.starcom.model.NextGenError
import be.hcpl.android.zengo.service.starcom.model.NextGenTransmit
import be.hcpl.android.zengo.service.starcom.model.NextGenUnit
import com.google.gson.Gson
import com.mikhaellopez.circularprogressbar.CircularProgressBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class OverviewFragment : Fragment(R.layout.fragment_overview) {

    private val service = NextGentServiceImpl().getService()
    private val gson = Gson()
    private lateinit var prefs: SharedPreferences

    // TODO move to resources
    private val demoJson =
        "{\"unitnumber\":\"123456\",\"name\":\"538ZFAZ74LCK00000\",\"unittype\":\"5\",\"unitmodel\":\"6\",\"mileage\":\"4875.26\",\"software_version\":\"190430\",\"logic_state\":\"2\",\"reason\":\"2\",\"response\":\"0\",\"driver\":\"0\",\"longitude\":\"2.349014\",\"latitude\":\"48.864716\",\"altitude\":\"52\",\"gps_valid\":\"0\",\"gps_connected\":\"0\",\"satellites\":\"0\",\"velocity\":\"0\",\"heading\":\"168\",\"emergency\":\"0\",\"shock\":\"\",\"ignition\":\"0\",\"door\":\"0\",\"hood\":\"0\",\"volume\":\"0\",\"water_temp\":\"\",\"oil_pressure\":\"0\",\"main_voltage\":13.18,\"analog1\":\"0.09\",\"siren\":\"0\",\"lock\":\"0\",\"int_lights\":\"0\",\"datetime_utc\":\"20191116090806\",\"datetime_actual\":\"20191116210319\",\"address\":\"Paris\",\"perimeter\":\"\",\"color\":2,\"soc\":64,\"tipover\":0,\"charging\":1,\"chargecomplete\":0,\"pluggedin\":1,\"chargingtimeleft\":120}"

    companion object {
        //const val KEY_IVSPEC = "iv_spec"
        const val KEY_USERNAME = "username"
        const val KEY_PASSWORD = "password"
        const val KEY_UNITNO = "unitno"
        const val KEY_UNITS_METRIC = "unit_prefs_metric"
        const val KEY_UNITS_IMPERIAL = "unit_prefs_imperial"
        const val KEY_SCREEN_ON = "keep_screen_on"
        const val DEMO = "demo"
        const val KEY_COLLAPSED = "collapsed"

        const val DEMO_UNIT_NO = 123456L
    }

    private lateinit var username: EditText
    private lateinit var password: EditText
    private lateinit var reveal: View
    private lateinit var clear: View
    private lateinit var save: View
    private lateinit var submit: View
    private lateinit var collapse: ImageView
    private lateinit var inputGroupView: View
    private lateinit var optionMetric: RadioButton
    private lateinit var optionImperial: RadioButton
    private lateinit var units: RadioGroup
    private lateinit var keepScreenOn: CheckBox
    private lateinit var unitNumber: TextView
    private lateinit var status: TextView
    private lateinit var serialnumber: TextView
    private lateinit var voltage: TextView
    private lateinit var mileage: TextView
    private lateinit var location: TextView
    private lateinit var timeLeft: TextView
    private lateinit var progressText: TextView
    private lateinit var progress: CircularProgressBar
    private lateinit var tipover: View
    private lateinit var charging: View
    private lateinit var charged: View
    private lateinit var plugged: View

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        username = view.findViewById(R.id.username)
        password = view.findViewById(R.id.password)
        reveal = view.findViewById(R.id.reveal)
        clear = view.findViewById(R.id.clear)
        save = view.findViewById(R.id.save)
        submit = view.findViewById(R.id.submit)
        collapse = view.findViewById(R.id.collapse)
        inputGroupView = view.findViewById(R.id.input_group)
        optionMetric = view.findViewById(R.id.option_metric)
        optionImperial = view.findViewById(R.id.option_imperial)
        units = view.findViewById(R.id.units)
        keepScreenOn = view.findViewById(R.id.keep_screen_on)
        unitNumber = view.findViewById(R.id.unitNumber)
        status = view.findViewById(R.id.status)
        serialnumber = view.findViewById(R.id.serialnumber)
        voltage = view.findViewById(R.id.voltage)
        mileage = view.findViewById(R.id.mileage)
        location = view.findViewById(R.id.location)
        tipover = view.findViewById(R.id.tipover)
        timeLeft = view.findViewById(R.id.time_left)
        progressText = view.findViewById(R.id.progress_text)
        progress = view.findViewById(R.id.progress)
        plugged = view.findViewById(R.id.plugged)
        charging = view.findViewById(R.id.charging)
        charged = view.findViewById(R.id.charged)

        onViewCreatedCont()
    }


    private fun onViewCreatedCont() {

        // get user preferences
        prefs = PreferenceManager.getDefaultSharedPreferences(requireContext())

        // restore input from these prefs
        // restore username from prefs
        prefs.getString(KEY_USERNAME, null)?.let { username.setText(it) }
        username.doAfterTextChanged {
            // after changing username; update name in prefs and remove unitNo so it's fetched again
            prefs.edit().putString(KEY_USERNAME, username.text.toString()).apply()
            clearUnitNo()
        }
        prefs.getString(KEY_PASSWORD, null)?.let { password.setText(it) }

        // store input
        save.setOnClickListener {
            prefs.edit().putString(KEY_USERNAME, username.text.toString())
                .putString(KEY_PASSWORD, password.text.toString()).apply()
            // again credentials changed so remove unitNo to get data again
            clearUnitNo()

        }
        // option to show current input password as clear text instead
        reveal.setOnClickListener {
            password.transformationMethod =
                if (password.transformationMethod == null) PasswordTransformationMethod() else null
        }
        // remove input
        clear.setOnClickListener {
            username.setText("")
            password.setText("")
            prefs.edit().remove(KEY_USERNAME).remove(KEY_PASSWORD).apply()
            // again credentials changed so remove unitNo to get data again
            clearUnitNo()
        }
        // get remote information
        submit.setOnClickListener {
            closeKeyboard()
            getUnitNumber()
        }
        // hide credentials
        collapse.setOnClickListener {
            // update in prefs also
            prefs.edit().putBoolean(KEY_COLLAPSED, inputGroupView.isVisible).apply()
            // apply on views
            if (inputGroupView.isVisible) {
                // collapse FIXME animation not working
                collapseLoginView()
            } else {
                // show contents again
                inputGroupView.animate().translationY(0f)
                inputGroupView.visibility = View.VISIBLE
                collapse.setImageResource(R.drawable.ic_keyboard_arrow_up)
            }
        }
        // also restore that value from preferences
        if (prefs.getBoolean(KEY_COLLAPSED, false)) {
            collapseLoginView()
        }

        // update and recover preferred metrics
        optionMetric.isChecked = prefs.getBoolean(KEY_UNITS_METRIC, true)
        optionImperial.isChecked = prefs.getBoolean(KEY_UNITS_IMPERIAL, false)
        units.setOnCheckedChangeListener { _, _ ->
            prefs.edit()
                .putBoolean(KEY_UNITS_METRIC, optionMetric.isChecked)
                .putBoolean(KEY_UNITS_IMPERIAL, optionImperial.isChecked)
                .apply()
            // no need to submit again, just update views
            updateMileageView()
        }
        // toggle keep screen on preference
        keepScreenOn.setOnCheckedChangeListener { _, isChecked ->
            prefs.edit().putBoolean(KEY_SCREEN_ON, isChecked).apply()
            if (isChecked)
                activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            else
                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
    }

    private fun clearUnitNo() {
        prefs.edit().remove(KEY_UNITNO).apply()
        unitNumber.text = ""
        (requireActivity() as MainActivity).state = MainActivity.State()
    }

    private fun collapseLoginView() {
        inputGroupView.animate().translationY(inputGroupView.height.toFloat())
        inputGroupView.visibility = View.GONE
        collapse.setImageResource(R.drawable.ic_keyboard_arrow_down)
    }

    override fun onResume() {
        super.onResume()
        // on resume check if all input is present and if so already submit for auto refresh
        if (username.text.isNotBlank() && password.text.isNotBlank()) {
            submit.callOnClick()
        }
        // update keep screen on preference here
        keepScreenOn.isChecked = prefs.getBoolean(KEY_SCREEN_ON, false)
    }

    private fun closeKeyboard() {
        activity?.currentFocus?.let { view ->
            (view.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                .hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun getUnitNumber() {
        // get state reference
        var state = (requireActivity() as MainActivity).state
        // get last use unitNo and set it to state
        val storedUnitNumber = prefs.getLong(KEY_UNITNO, 0L)
        if (storedUnitNumber != 0L) {
            state.unit?.let {
                state = state.copy(unit = it.copy(unitnumber = storedUnitNumber))
            } ?: run {
                state = state.copy(unit = NextGenUnit(unitnumber = storedUnitNumber))
            }
            (requireActivity() as MainActivity).state = state
        }
        // skip if possible
        state.unit?.unitnumber?.let { showUnitNumber(it) }
        if (state.unit?.unitnumber == null || state.unit?.unitnumber == 0L) {

            // demo login
            if (isDemo()) {
                state = state.copy(unit = NextGenUnit(unitnumber = DEMO_UNIT_NO))
                showUnitNumber(state.unit?.unitnumber ?: DEMO_UNIT_NO)
                return
            }

            // get unit number from first service using these values
            // needed? showLoading()
            service.getUnits(
                user = username.text.toString(),
                pass = password.text.toString()
            ).enqueue(object : Callback<List<NextGenUnit>> {

                override fun onResponse(
                    call: Call<List<NextGenUnit>>,
                    response: Response<List<NextGenUnit>>
                ) {

                    if (!response.isSuccessful) {
                        val message = gson.fromJson(
                            response.errorBody()?.charStream(),
                            NextGenError::class.java
                        )
                        status.text = getString(R.string.err_failed_loading, message.error)
                    } else {
                        handleReceivedUnit(response.body())
                    }
                }

                override fun onFailure(call: Call<List<NextGenUnit>>, t: Throwable) {
                    status.text = getString(R.string.err_failed_loading, "")
                }
            })
        }
    }

    private fun isDemo() = DEMO == username.text.toString() && DEMO == password.text.toString()

    private fun handleReceivedUnit(response: List<NextGenUnit>?) {
        // get state reference
        var state = (requireActivity() as MainActivity).state
        response?.let {
            // store that unitNumber in preferences also so we don't have to get this again
            it.firstOrNull()?.unitnumber?.let {
                prefs.edit().putLong(KEY_UNITNO, it).apply()
            }
            // update state
            state = state.copy(unit = it.firstOrNull())
            (requireActivity() as MainActivity).state = state
            state.unit?.unitnumber?.let { showUnitNumber(it) }
        }
    }

    private fun showUnitNumber(number: Long) {
        unitNumber.text = getString(R.string.label_your_unitNumber, number)
        continueWithUnitNumber(number)
    }

    private fun continueWithUnitNumber(unitNumber: Long) {

        // demo login
        if (isDemo()) {
            val response = gson.fromJson(demoJson, NextGenTransmit::class.java)
            handleReceivedLastTransmit(listOf(response))
            return
        }

        // with unit number get all other details
        service.lastTransmit(
            user = username.text.toString(),
            pass = password.text.toString(),
            unitNumber = unitNumber
        ).enqueue(object : Callback<List<NextGenTransmit>> {

            override fun onResponse(
                call: Call<List<NextGenTransmit>>,
                response: Response<List<NextGenTransmit>>
            ) {

                if (!response.isSuccessful) {
                    val message =
                        gson.fromJson(response.errorBody()?.charStream(), NextGenError::class.java)
                    status.text = getString(R.string.err_failed_loading, message.error)
                } else {
                    handleReceivedLastTransmit(response.body())
                }
            }

            override fun onFailure(call: Call<List<NextGenTransmit>>, t: Throwable) {
                status.text = getString(R.string.err_failed_loading, "")
            }
        })
    }

    private fun handleReceivedLastTransmit(body: List<NextGenTransmit>?) {
        body?.firstOrNull()?.let { transmit ->
            // update state
            activity?.let {
                (it as MainActivity).state = it.state.copy(transmit = transmit)
                // update views
                serialnumber.text =
                    getString(R.string.label_serial_number, transmit.name)
                voltage.text =
                    getString(R.string.label_main_voltage, transmit.main_voltage)
                updateMileageView()

                updateProgressBar(transmit)
                updateChargeState(transmit)
                updateLocation(transmit)
                val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
                sdf.timeZone = Calendar.getInstance().timeZone
                val date = transmit.datetime_actual?.let { actual ->
                    SimpleDateFormat("yyyyMMddHHmmss").parse(actual)
                }
                date?.let { validDate ->
                    status.text = getString(R.string.data_loaded, sdf.format(validDate))
                }
            }
        }
    }

    private fun updateMileageView() {
        mileage.text = getString(
            R.string.value_mileage,
            (requireActivity() as MainActivity).state.transmit?.mileage
        )
            .plus(" ").plus(
                if (optionMetric.isChecked) getString(R.string.unit_kilometers)
                else getString(R.string.unit_miles)
            )
    }

    private fun updateLocation(transmit: NextGenTransmit) {
        location.text = transmit.address
        tipover.alpha = if (transmit.tipover == 1) 1f else 0.2f
    }

    private fun updateChargeState(transmit: NextGenTransmit) {
        charging.alpha = if (transmit.charging == 1) 1f else 0.2f
        plugged.alpha = if (transmit.pluggedin == 1) 1f else 0.2f
        charged.alpha = if (transmit.chargecomplete == 1) 1f else 0.2f
        timeLeft.alpha = if (transmit.charging == 1) 1f else 0.2f
        timeLeft.text = getString(R.string.value_charging_time_left, transmit.chargingtimeleft)
    }

    private fun updateProgressBar(transmit: NextGenTransmit) {
        progressText.text = getString(R.string.value_soc, transmit.soc)
        progressText.visibility = View.VISIBLE
        progress.apply {
            // or with animation
            setProgressWithAnimation(transmit.soc?.toFloat() ?: 100f, 1000) // =1s
            // Set Progress Max
            progressMax = 100f
        }
    }

}
