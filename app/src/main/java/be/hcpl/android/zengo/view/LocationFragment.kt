package be.hcpl.android.zengo.view

import android.content.SharedPreferences
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.TextUtils
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.CheckBox
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import be.hcpl.android.zengo.BuildConfig.FORECAST_API_KEY
import be.hcpl.android.zengo.BuildConfig.FORECAST_SERVICE_URL
import be.hcpl.android.zengo.R
import be.hcpl.android.zengo.service.forecast.ForecastService
import be.hcpl.android.zengo.service.forecast.ForecastServiceImpl
import be.hcpl.android.zengo.service.forecast.Units
import be.hcpl.android.zengo.service.forecast.model.ForecastResponse
import be.hcpl.android.zengo.service.starcom.NextGentServiceImpl
import be.hcpl.android.zengo.service.starcom.model.NextGenError
import be.hcpl.android.zengo.service.starcom.model.NextGenHistory
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class LocationFragment : Fragment(R.layout.fragment_location), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private var currentLocationName: String? = null
    private var currentLocationLat: Double? = null
    private var currentLocationLong: Double? = null
    private var currentRange: Double? = null

    private lateinit var forecastService: ForecastService
    private val service = NextGentServiceImpl()
        .getService()
    private val gson = Gson() // used for parsing error response only
    private lateinit var prefs: SharedPreferences

    // reference rendered routes so we can remove them to clear up map
    private val routes = emptyList<Polyline>().toMutableList()

    // route rendering properties
    private var strokeColor: Int = Color.BLUE
    private var strokeWith: Float = 2f
    private var circleColor: Int = Color.RED
    private var circleFillColor: Int = 0x30ff0000
    private var circleStrokeWidth: Float = 2f

    // random for color generation
    private var rnd = Random()

    // keep track of distance for all combined routes
    private var calculatedDistance = 0f
    private var metric = true
    private var displayUnit = "km"
    private var displayDistance = 0f

    private lateinit var totalDistanceView: TextView
    private lateinit var currentLocationView: TextView
    private lateinit var clearView: View
    private lateinit var infoHistoryView: TextView
    private lateinit var collapseView: ImageView
    private lateinit var randomColorsView: CheckBox
    private lateinit var dateView: DatePicker
    private lateinit var temperatureView: TextView
    private lateinit var weatherIconView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        forecastService = ForecastServiceImpl(FORECAST_SERVICE_URL).getService()
        prefs = PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    override fun onResume() {
        super.onResume()
        // update preferred unit
        metric = prefs.getBoolean(OverviewFragment.KEY_UNITS_METRIC, true)
        displayUnit =
            if (metric) getString(R.string.unit_kilometers) else getString(R.string.unit_miles)
        displayDistance =
            if (metric) calculatedDistance else calculatedDistance * RangeFragment.CONVERT_KM_TO_MILES.toFloat()
        // print calculated distance
        totalDistanceView.text = getString(R.string.total_distance, displayDistance, displayUnit)
        // TODO also restore calculated routes here? or not?

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        totalDistanceView = view.findViewById(R.id.total_distance)
        currentLocationView = view.findViewById(R.id.current_location)
        clearView = view.findViewById(R.id.clear)
        infoHistoryView = view.findViewById(R.id.info_history)
        collapseView = view.findViewById(R.id.collapse)
        randomColorsView = view.findViewById(R.id.random_colors)
        dateView = view.findViewById(R.id.date_history)
        temperatureView = view.findViewById(R.id.temperature)
        weatherIconView = view.findViewById(R.id.weather_icon)

        strokeColor = ContextCompat.getColor(requireContext(), R.color.strokeColor)
        strokeWith = resources.getDimension(R.dimen.map_stroke_width)
        circleColor = ContextCompat.getColor(requireContext(), R.color.circleColor)
        circleFillColor = ContextCompat.getColor(requireContext(), R.color.circleFillColor)
        circleStrokeWidth = resources.getDimension(R.dimen.map_stroke_width)

        val state = (requireActivity() as MainActivity).state
        currentLocationName = state.transmit?.address.orEmpty()
        currentLocationLat = state.transmit?.latitude
        currentLocationLong = state.transmit?.longitude
        currentRange = state.calculatedRange

        currentLocationView.text = getString(R.string.current_location_is, currentLocationName)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        // get weather for location
        getWeatherForLocation()

        // set current date on date picker (done by default)
        val today = Calendar.getInstance()
        dateView.init(
            today.get(Calendar.YEAR),
            today.get(Calendar.MONTH),
            today.get(Calendar.DAY_OF_MONTH)) {

                _, year, monthOfYear, dayOfMonth -> handleDateUpdate(year, monthOfYear, dayOfMonth)
        }
        infoHistoryView.setOnClickListener { collapseDateSelection() }
        collapseView.setOnClickListener { collapseDateSelection() }
        // removes all rendered routes
        clearView.setOnClickListener {
            routes.forEach {
                it.remove()
            }
            routes.clear()
            calculatedDistance = 0f
            displayDistance = 0f
            totalDistanceView.text = getString(R.string.total_distance, displayDistance, displayUnit)

        }

        // restore random colors option
        randomColorsView.isChecked = prefs.getBoolean(PREF_RANDOM_COLORS_ROUTE, true)
        randomColorsView.setOnCheckedChangeListener { _, isChecked ->
            prefs.edit().putBoolean(PREF_RANDOM_COLORS_ROUTE, isChecked).apply()
        }
    }

    private fun handleDateUpdate(year: Int, monthOfYear: Int, dayOfMonth: Int) {
        // retrieve information for given selected date
        val formattedMonth = if (monthOfYear + 1 < 10) "0${monthOfYear+1}" else "${monthOfYear+1}"
        val formattedDay = if (dayOfMonth < 10) "0${dayOfMonth}" else "${dayOfMonth}"
        val unitNo = prefs.getLong(OverviewFragment.KEY_UNITNO, OverviewFragment.DEMO_UNIT_NO)
        val username = prefs.getString(OverviewFragment.KEY_USERNAME, OverviewFragment.DEMO)
        val password = prefs.getString(OverviewFragment.KEY_PASSWORD, OverviewFragment.DEMO)

        if (isDemo(username.orEmpty(), password.orEmpty())) {

            val demoData = listOf(
                NextGenHistory(latitude = "48.864716", longitude = "2.349014"),
                NextGenHistory(latitude = "48.064716", longitude = "2.549014"),
                NextGenHistory(latitude = "47.864716", longitude = "2.049014"),
                NextGenHistory(latitude = "48.064716", longitude = "2.149014"),
                NextGenHistory(latitude = "48.464716", longitude = "2.249014"),
                NextGenHistory(latitude = "48.864716", longitude = "2.349014")
            )
            handleReceivedHistory(
                "${year}${formattedMonth}${formattedDay}",
                demoData
            )

        } else {

            service.getHistory(
                unitNo,
                username.orEmpty(),
                password.orEmpty(),
                "${year}${formattedMonth}${formattedDay}000000",
                "${year}${formattedMonth}${formattedDay}235959"
            ).enqueue(object : Callback<List<NextGenHistory>> {


                override fun onFailure(call: Call<List<NextGenHistory>>, t: Throwable) {
                    infoHistoryView.text =
                        getString(R.string.err_failed_loading, t.localizedMessage)
                }

                override fun onResponse(
                    call: Call<List<NextGenHistory>>,
                    response: Response<List<NextGenHistory>>
                ) {

                    if (!response.isSuccessful) {
                        val message = gson.fromJson(
                            response.errorBody()?.charStream(),
                            NextGenError::class.java
                        )
                        infoHistoryView.text =
                            getString(R.string.err_failed_loading, message.error)
                    } else {
                        infoHistoryView.text = getString(R.string.info_map_history)
                        handleReceivedHistory(
                            "${year}${formattedMonth}${formattedDay}",
                            response.body()
                        )
                    }
                }
            })
        }
    }

    private fun isDemo(username: String, password: String) =
        OverviewFragment.DEMO == username && OverviewFragment.DEMO == password

    private fun collapseDateSelection() {
        // TODO implement proper animation here
        if (dateView.isVisible) {
            dateView.visibility = GONE
            randomColorsView.visibility = GONE
            clearView.visibility = GONE
            collapseView.setImageResource(R.drawable.ic_keyboard_arrow_up)
        } else {
            dateView.visibility = VISIBLE
            randomColorsView.visibility = VISIBLE
            clearView.visibility = VISIBLE
            collapseView.setImageResource(R.drawable.ic_keyboard_arrow_down)
        }
    }

    private fun handleReceivedHistory(date: String, list: List<NextGenHistory>?) {
        // renders a line on map for all the points retrieved from history service
        val polyLineOptions = PolylineOptions().clickable(true)
        var previousLocation: Location? = null
        list?.forEach {
            if (!it.latitude.isNullOrBlank() && !it.longitude.isNullOrBlank()) {
                val currentLocation = Location("").apply {
                    latitude = it.latitude.toDouble()
                    longitude = it.longitude.toDouble()
                }
                if (previousLocation != null) {
                    // update (unit is meters so change to km)
                    calculatedDistance += (previousLocation as Location).distanceTo(currentLocation) / 1000
                }
                previousLocation = currentLocation
                polyLineOptions.add(LatLng(it.latitude.toDouble(), it.longitude.toDouble()))
            }
        }
        val polyline: Polyline = map.addPolyline(polyLineOptions)
        // Store a data object with the polyline, used here to indicate an arbitrary type.
        polyline.tag = date // uses date as a tag on the lines
        polyline.width = strokeWith
        polyline.color = nextRandomColor()
        polyline.jointType = JointType.ROUND
        // keep reference so we can remove lines from previous dates
        routes.add(polyline)
        // update total distance
        displayDistance =
            if (metric) calculatedDistance else calculatedDistance * RangeFragment.CONVERT_KM_TO_MILES.toFloat()
        totalDistanceView.text = getString(R.string.total_distance, displayDistance, displayUnit)
    }

    private fun nextRandomColor() =
        if (randomColorsView.isChecked)
            Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        else
            strokeColor

    private fun getWeatherForLocation() {
        forecastService.forecast(
            FORECAST_API_KEY,
            currentLocationLat,
            currentLocationLong,
            Units.AUTO
        ).enqueue(object :
            Callback<ForecastResponse> {

            override fun onResponse(
                call: Call<ForecastResponse>,
                response: Response<ForecastResponse>
            ) {
                if (!response.isSuccessful) {
                    currentLocationView.text = getString(R.string.err_loading_weather_info)
                } else {
                    response.body()?.let { forecast -> handleWeatherInfo(forecast) }
                }
            }

            override fun onFailure(call: Call<ForecastResponse>, t: Throwable) {
                currentLocationView.text = getString(R.string.err_loading_weather_info)
            }
        })
    }

    private fun handleWeatherInfo(forecastResponse: ForecastResponse) {

        // implement images using http://adamwhitcroft.com/climacons/
        weatherIconView.setImageResource(mapImageToResource(forecastResponse))

        // display temp
        temperatureView.text = forecastResponse.currently?.temperature?.toInt().toString()

        currentLocationView.text = getString(
            R.string.current_location_and_weather,
            currentLocationName,
            forecastResponse.currently?.summary
        )

    }

    private fun mapImageToResource(response: ForecastResponse?): Int {
        // return a default image if no valid input is given
        if (response?.daily == null || TextUtils.isEmpty(response.daily?.icon)) {
            return R.drawable.partly_cloudy_day
        }
        val weatherIconDescription = response.daily?.icon
        return if ("clear-day" == weatherIconDescription) {
            R.drawable.clear_day
        } else if ("clear-night" == weatherIconDescription) {
            R.drawable.clear_day
        } else if ("rain" == weatherIconDescription) {
            R.drawable.rain
        } else if ("snow" == weatherIconDescription) {
            R.drawable.snow
        } else if ("sleet" == weatherIconDescription) {
            R.drawable.sleet
        } else if ("wind" == weatherIconDescription) {
            R.drawable.wind
        } else if ("fog" == weatherIconDescription) {
            R.drawable.fog
        } else if ("cloudy" == weatherIconDescription) {
            R.drawable.cloudy
        } else if ("partly-cloudy-night" == weatherIconDescription) {
            R.drawable.partly_cloudy_night
        } else
        // used as default
        {
            R.drawable.partly_cloudy_day
        }//		else if ("partly-cloudy-day".equals(weatherIconDescription))
        //			return R.drawable.partly_cloudy_day;
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        // get current bikes location here
        currentLocationLat?.let { lat ->
            currentLocationLong?.let { long ->
                // get bikes current location
                val bikesLocation = LatLng(lat, long)

                // put in on map
                map.addMarker(MarkerOptions().position(bikesLocation).title(getString(R.string.poi_your_bike)))
                map.moveCamera(CameraUpdateFactory.newLatLng(bikesLocation))


                // draw a range circle
                currentRange?.let { range ->
                    drawCircle(bikesLocation, range * 1000) // in meters
                }
            }
        }
    }

    private fun drawCircle(point: LatLng, radius: Double) {
        // Instantiating CircleOptions to draw a circle around the marker
        val circleOptions = CircleOptions()
        // Specifying the center of the circle
        circleOptions.center(point)
        // Radius of the circle, specified in meters
        circleOptions.radius(radius)
        // Border color of the circle
        circleOptions.strokeColor(circleColor)
        // Fill color of the circle
        circleOptions.fillColor(circleFillColor)
        // Border width of the circle
        circleOptions.strokeWidth(circleStrokeWidth)

        // Adding the circle to the GoogleMap
        val circle = map.addCircle(circleOptions)
        // also get a zoom level based on that value
        val zoomLevel = getZoomLevel(circle)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, zoomLevel.toFloat()))

    }

    fun getZoomLevel(circle: Circle?): Int {
        var zoomLevel = 10
        if (circle != null) {
            val radius = circle.radius
            val scale = (radius / 500) * 1.2 // have a slightly bigger zoom than radius (1.2)
            zoomLevel = (16 - Math.log(scale) / Math.log(2.0)).toInt()
        }
        return zoomLevel
    }

    companion object {

        val PREF_RANDOM_COLORS_ROUTE = "random_colors_route"

    }

}
