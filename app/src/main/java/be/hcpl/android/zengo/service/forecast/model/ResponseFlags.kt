package be.hcpl.android.zengo.service.forecast.model

import androidx.annotation.Keep

@Keep
class ResponseFlags {

    var flags: Array<String>? = null
    // can't parse the isd stations
    //private String[] isd-stations;
    // can't pars the metno-license
    //private String metno-license;
    //"Based on data from the Norwegian Meteorological Institute. (http://api.met.no/)",
    // can't parse metar-stations
    //private String [] metar-stations;
    var units: String? = null

}
