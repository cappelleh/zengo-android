package be.hcpl.android.zengo.service.starcom

import be.hcpl.android.zengo.service.starcom.model.NextGenHistory
import be.hcpl.android.zengo.service.starcom.model.NextGenTransmit
import be.hcpl.android.zengo.service.starcom.model.NextGenUnit
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface NextGentService {

    // starcom systems API

    @GET("mongol/api.php?commandname=get_units&format=json")
    fun getUnits(
        @Query("user") user: String,
        @Query("pass") pass: String
    ): Call<List<NextGenUnit>>

    @GET("mongol/api.php?commandname=get_last_transmit&format=json")
    fun lastTransmit(
        @Query("unitnumber") unitNumber: Long,
        @Query("user") user: String,
        @Query("pass") pass: String
    ): Call<List<NextGenTransmit>>

    @GET("mongol/api.php?commandname=get_history&format=json")
    fun getHistory(
        @Query("unitnumber") unitNumber: Long,
        @Query("user") user: String,
        @Query("pass") pass: String,
        @Query("start") start: String,
        @Query("end") end: String
    ): Call<List<NextGenHistory>>

}

class NextGentServiceImpl() {

    fun getService(): NextGentService {
        return getRetrofit("https://mongol.brono.com/").create(NextGentService::class.java)
    }

    private fun getRetrofit(url: String): Retrofit {
        val gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }
}
