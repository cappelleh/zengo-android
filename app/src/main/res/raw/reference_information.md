
# Zero SR Platform Owners Information

## Zero Platforms breakdown

**X-platform** is the original platform. These are dirtbikes with a lower weight and removable battery bricks. The idea here was that one could race it with as much battery modules as needed and then have another set of battery modules charging. So that once depleted the battery could be swapped out. 

**S-platform** is the original street platform. These are street oriented bikes. It's available with different battery arrangements but these are fixed to the bike. By default charging is done with a computer like cable. An optional type2 charger (EU bikes) can be added for faster charger or the tank can be replaced with an extra fixed 3.6kWh battery pack. 

**SR-platform** is the latest street platform with a newly designed frame and higher power motor and controller. These come as standard with the 14.4kWh battery and either a 3kW or 6kW (premium) on board charger. The tank storage can be configured with either an extra 6kW charger or an extra 3.6kWh battery module (also fixed in place).

And then there are many variations based on these platforms. 

The more offroad oriented **DS model** with higher suspension travel, a more upright sitting position, different foot pegs and dual sport oriented tyres. Motor and controller are identical.

The **R variation** is available for both the S and DS (called SR and DSR) and consist of a wider belt (to handle the extra power), the same motor but with a higher output motor controller for more power. For the DSR model there is a special Black Forest edition (BF) that comes with lots of upgrades like a screen, luggage solution and charge tank installed from the factory. 

The **FXS model** is an FX with street wheels, a so called super motard. 

The **SR/F** is the first version on the SR platform as a naked, streetfighter bike with a sporty position having the foot pegs rather high, short rear seat and forward handlebars. 

The latest model added to the line-up is the **SR/S** also based on the SR platform but this time with a fairing added, more upright seating position thanks to higher up and closer handlebars and lowered footpegs. The rear section is also enlarged including a larger pillion seat and a luggage rack included by default (the SR/F requires a zero accessoiry for adding the luggage rack for top case mounting).

## Belt Tension Check Tools & Values

Belt Tension can be checked with the **Gates Carbon Drive App**. Make sure to use it in a completely silent environment. Stringing the belt should then give you a reading **between 62 Hz and 82 Hz**. 

Alternative option is to use the **Gates 91107** tool giving a reading **between 51 kg and 102 kg**.

For more information visit [https://zeromanual.com/wiki/Belt_Tension_Check](https://zeromanual.com/wiki/Belt_Tension_Check)

## Changing Belt tension

Loosen rear wheel nut using a **27mm socket**. Once loose adjust the wheel by turning the set screws with a **Torx 45 socket turning clockwise** (for higher tension) in quarter of a turn at once. Make sure to adjust both sides identical to keep wheel alignment. 

Once done fasten wheel nut again making sure the locking block on the other side is in place and for no more than **102Nm or 75 lb-ft**. 

**Tip**: If you don't have a torque wrench you can mark the original nut position by marking both bolt and nut. When refitting you then just have to make sure these marks align again.  

## Tyre Pressure & Size

Both front and rear tyres have a recommend inflation of **248 kPa or 36 PSI**. Measured on cold tyres. Tyres are cold when they haven't been used for at least 3 hours.  

```
Front | Pirelli Diable Rosso ||| **120/70-17**
Rear  | Pirelli Diablo Rosso ||| **180/55-17**
```

## Maintenance Parts

Some maintenance parts and their specifications. 

```
| Part             | Spec.       |
|------------------|-------------|
| brake fluid      | DOT 4       |
| Indicator bulbs  | RY10W       |
| Brake pads front | 352101NTO   |
|                  | SBS-SI-80GG |
| Brake pads rear  | 352061NTO   |
|                  | SBS-SI-24HH |
```

## Maintenance Specifications & Intervals

Minimal brake rotor sizes.

```
| Rotor   | Minimal Thickness    |
|---------|----------------------|
| Front   | 0,18 inch or 4,5 mm  |
| Rear    | 0,16 inch or 4,0 mm  |
```

See Zero's online documentation for this [https://www.zeromotorcycles.com/owner-resources/](https://www.zeromotorcycles.com/owner-resources/)

## Firmware Upgrades

When a firmware update is available your bike will show a warning on it's display when you turn it on without a charger connected. The warning doesn't show when a charger is connected. The pop-up is similar to the pop-up you see asking for override of menu when you toggle the ignition switch with a charger plugged in. 

From that firmware version update pop-up you can choose to install it right now or to postpone the update. If you choose to install it you have to leave the bike on as long as the update takes. The SOC indication on the bike (circle) is used to show the progress. Updates typically take around 30 minutes. After the update the bike just reboots and stays on with the ignition on and lights on like it would otherwise do. 

If you choose to postpone it will only ask again the next day. If you need to trigger the update yourself you can now use the app to check and update from there. This is only available since firmware version 14. So the first updates have to be done over the air.  

To update from the app you need your phone connected to the bike and have it on and connected during the update. (TODO indicate where in the app the update information can be found)

An overview of firmware versions that have been released by Zero can be found online. Note that this reference is typically at least one version behind. 

[https://www.zeromotorcycles.com/owner-resources/firmware/](https://www.zeromotorcycles.com/owner-resources/firmware/)

## User Registration

### Initial Registration

When you get the bike from the dealer you need to register it with your own user details before you can connect to it using bluetooth. To do so download the official Zero Nextgen App on your device. It will guide you on how to register and after that also how to connect the device to your bike. 

During registration you'll be asked for an e-mail address and a password. These details are what is linked to your bike and will be used as login for both the Zero app and the Remote connection that this app uses. 

### Changing Registration Details

Once registered your bike is fixed to that account and that can only be changed by your Zero dealer or by contacting Zero customer support yourself.

That could be needed if you get a Zero second hand or, like some users already reported, when you get a demo bike from a dealer.

### Password Reset

Within the official Zero App there is no way to reset your password. You can only register and deregister which only refers to the credentials stored in the app. This has no effect on the user account linked to a specific bike.

If you forgot your password there is a way to change that within the app though. These are the steps to follow, [https://www.electricmotorcycleforum.com/boards/index.php?topic=10072.msg90912#msg90912](quoted from here)

```
While being connected to the bike via bluetooth, I hit the Deregister button in the account menu. I got a message that I am not deregistered and then I registered again. This worked with the same E-Mail address I used before and I was able to set a new password. Now I can connect again, also with the zeroNG app!
It seems that deregister and register again is the way to change a password.
```

If you have this app installed there is also a somewhat "hidden" trick to show your current password. For that just double tap the save button next to the password field and it will change from hidden input to clear text revealing current input. Obviously this only works if you saved the (right) password before. 

## Recalls

### Tank lid replacement

From what I've discovered so far there is only a single official recall available on the SR/F so far. And it's only applicable to the first bikes that were delivered. Later bakes come with a fix form the factory (as does the SR/S).

The issue is the tank lid being lifted up at the front of the bike leaving a gap with the rest of the fairing and not aligning properly. The fix consist of a replacement latch that can either be installed by your dealer or (as some reported) by yourself. 

The instructions on how to replace the lid can be found on [https://www.electricmotorcycleforum.com/boards/index.php?topic=9929.0](the electricmotorcycles forum). Including pictures showing the issue on my personal SR/F delivered in August 2019. 

## VIN Breakdown

Zero's motorcycle VIN numbers consists of the following information.

```
538 = Zero Motorcycles Inc.
Z = Z4 - FST Platform
FA = 20 MY SR/F (FB for SR/S)
Z7 = 40 kW (54 HP)
0 = Check Digit (calculated)
L = 2020
C = Scotts Valley, CA, USA
K = SR/F
12345 = Production Serial Number
```

Thanks to [https://www.electricmotorcycleforum.com/boards/index.php?topic=9520.msg93182#msg93182](contributions to this topic) we figured out that the **FA** digits found within the SR/F VIN is replaced by **FB** on SR/S motorcycles.

## 12V System Reset

If needed you can reset the system by disconnecting the 12V battery found underneath the main seat. 

To remove the main seat you first need to remove the passenger seat using the ignition key on the 
lock and the left side of the main seat. 

This reveals a **Torx 30** bolt that retains the main seat. Remove that and put the main seat aside. 

Now there are 2 **Torx 25** bolts left on the plastic part covering the lock underneath the main seat at the left side. Remove these and slide the plastic cover away to reveal the 12V battery. 

With a **10mm socket** you can now loosen the nut on the negative pole of the battery. Always remove/add the negative pool first/last when connection/disconnecting the 12V battery. The positive pole is covered with a red plastic.  

## 12V Fuses

Fuses are located in the fuse box besides the 12V battery below the main seat. Numbers counting from top to bottom.

```
| Fuse NÂ° | Value | circuit      |
|----------|-------|--------------|
| 1        | 5A    | Celmodule    |
| 2        | 10A   | ABS-9 valve  |
| 3        | 10A   | PDU          |
| 4        | 10A   | PDU          |
| 5        | 10A   | PDU          |
| 6        | 5A    | Dashboard    |
| 7        | 25A   | ABS-18 motor |
```

There are also 2 high voltage fuses on the bike to be found underneath the tank at the left side of the motorcycle visible between the trellis frame covered with a black piece of plastic. These are **SPT3, 15A** fuses for the DC/DC converter and BMS. 

## Long Term Storage

It's recommended to keep the battery at **60% SoC** for long term storage without keeping it plugged in. 

After 30 days without switching the bike on it will automatically go into **long storage mode**. This will drain the battery to 60% if it was left at a higher SoC and also prevent charging for more than 60%.

Avoid having the battery below 30% for longer periods of time. Plug the bike back in when it goes below 30% and charge back up to 60%.  

Recommended storage room temperature ranges from **-4° F or -20° C** to **95° F or 35° C**.

Disable long term storage mode by briefly switching the bike ON and OFF again. Then charge for 24u to get the battery back in full charge state.  

## About This App

Android and iOS App to connect with public API for Zero SR/F next gen motorcycle information. This builds upon the data you see when using the "remote connect" functionality in the app.

Android app is available from the Google Play Store, see [https://play.google.com/store/apps/details?id=be.hcpl.android.zengo](https://play.google.com/store/apps/details?id=be.hcpl.android.zengo)

iOS app is available from this link [https://apps.apple.com/be/app/zerong/id1488172044](https://apps.apple.com/be/app/zerong/id1488172044)

Project website for this app available at [https://bitbucket.org/cappelleh/zengo/](https://bitbucket.org/cappelleh/zengo/)
